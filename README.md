# [Wifi to QrCode](https://gitlab.com/squall77it/wifi-qrcode)

[![build status](https://gitlab.com/squall77it/wifi-qrcode/badges/master/build.svg)](https://gitlab.com/squall77it/wifi-qrcode/commits/master)
<!--[![coverage report](https://gitlab.com/squall77it/wifi-qrcode/badges/master/coverage.svg?job=coverage)](https://gitlab.com/squall77it/wifi-qrcode/commits/master)
-->
[![License: GPL v3](https://img.shields.io/badge/license-GPL%20v3-blue.svg)](http://www.gnu.org/licenses/gpl-3.0)

Simple application to generate a QrCode from Wifi Connections

- [About](#about)
- [Requirements](#requirements)
  - [Runtime](#runtime)
  - [Development](#development)
- [Installation](#installation)

<!--  - [Official distribution packages](#official-distribution-packages)
  - [Flatpak](#flatpak)
  - [Arch Linux](#arch-linux)
  - [Ubuntu](#ubuntu)
  - [Debian](#debian)
  - [Fedora](#fedora) -->

  - [From source](#from-source)


<!--- [Frequently Asked Questions](#frequently-asked-questions)
  - [How can I ...?](#how-can-i-...)
- [Contribute](#contribute)
  - [Development](#development-1)
  - [Translations](#translations)
 -->
- [License](#license)



## About
A small project to create a QrCode from your WiFi configuration.
<!--- such that you save yourself from typing in long passwords on your, say, mobile. 
Wifi QrCode creates ......
-->

## Requirements
Minimun requirements is from Debian Jessie

### Runtime

- libgtk-3-0 (>= 3.14.5)
- libglib2.0-0 (>= 2.42.1)
- libqrencode3
- A window manager


### Development

- valac-0.26 (>= 0.26.1)
- libvala-0.26-dev (>= 0.26.1)
- cmake (>= 3.0.2)
- libxml2-util (>= 2.9.1)
- libglib2.0-dev (>= 2.42.1)
- libgtk-3-dev (>= 3.14.5)
- libqrencode-dev


## Installation
### From source
```shell
git clone https://gitlab.com/squall77it/wifi-qrcode.git
cd wifi-qrcode
mkdir install
cd install
cmake ..
make
sudo make install
```

You can uninstall Wifi to QrCode again by running `sudo make uninstall` from within the
build directory.


## License
Wifi to QrCode - Copyright (c) 2017 by Gianni Lerro {Squall77it} ~ <squall77it@gmail.com>

Wifi to QrCode is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Wifi to QrCode is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Wifi to QrCode.  If not, see <http://www.gnu.org/licenses/>.

