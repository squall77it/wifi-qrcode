/* -*- Mode: Vala; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*- */
/*
 * This file is part of WifiQrCode.
 *
 * application.vala
 *
 * Copyright (c) 2017-2018 Gianni Lerro {Squall77it} ~ <squall77it@gmail.com>
 *
 * WifiQrCode is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * WifiQrCode is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GLib;
using Gtk;

namespace WifiQrCode
{
	public class Application : Gtk.Application
	{
		private ApplicationWindow main_window;

		private const GLib.ActionEntry app_entries[] =
		{
			{ "about", on_about },
			{ "quit", on_quit }
		};

		public Application ()
		{
			GLib.Object (application_id: "org.gnome.wifi-qrcode",
				flags: ApplicationFlags.FLAGS_NONE);
		}

/*
		protected override void startup ()
		{
			base.startup ();
		}
/*

/*
		protected override void shutdown ()
		{
			base.shutdown ();
		}
*/
		protected override void activate ()
		{
			base.activate ();

			main_window = new ApplicationWindow (this);
			main_window.show_all ();
		}

		protected override void startup ()
		{
			base.startup ();

			add_action_entries (app_entries, this);
		}

		private void on_about (GLib.SimpleAction action, GLib.Variant? parameter)
		{

			string[] authors = {"Gianni Lerro <squall77it@gmail.com>"};
			string[] artists = {"Gianni Lerro <squall77it@gmail.com>"};
			string[] documenters = null;

			Gtk.show_about_dialog (main_window,
				program_name: _("Wifi to QrCode"),
				copyright: "Copyright \xc2\xa9 2017-2018 Gianni Lerro",
				comments: _("Application to generate a QrCode from a Wifi Connection"),
				authors: authors,
				artists: artists,
				documenters: documenters,
				/* Translators should localize the following string
				   which will be displayed at the bottom of the about
				   box to give credit to the translator(s). */
				translator_credits: _("translator-credits"),
				logo_icon_name: "org.gnome.wifi-qrcode",
				website: "https://gitlab.com/squall77it/wifi-qrcode",
				website_label: _("Website"),
				version: Constants.VERSION,
				license_type: License.GPL_3_0);
		}

		private void on_quit (GLib.SimpleAction action, GLib.Variant? parameter)
		{
			main_window.destroy ();
		}

	}
}

