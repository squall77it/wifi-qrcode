/* -*- Mode: Vala; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*- */
/*
 * This file is part of WifiQrCode.
 *
 * wifi-qrcode.vala
 *
 * Copyright (c) 2017 Gianni Lerro {Squall77it} ~ <squall77it@gmail.com>
 *
 * WifiQrCode is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * WifiQrCode is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// project version=0.20

using GLib;

namespace WifiQrCode {

	public static int main (string[] args)
	{

//		Intl.setlocale(LocaleCategory.ALL, "");
//		Intl.bindtextdomain(Constants.GETTEXT_PACKAGE, Path.build_filename(Constants.DATADIR,"locale"));
//		Intl.bind_textdomain_codeset(Constants.GETTEXT_PACKAGE, "UTF-8");
//		Intl.textdomain(Constants.GETTEXT_PACKAGE);

		Environment.set_prgname ("wifi-qrcode");
		Environment.set_application_name (_("Wifi to QrCode"));

		var application = new WifiQrCode.Application ();

		return application.run (args);
	}
}

