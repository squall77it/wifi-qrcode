/* -*- Mode: Vala; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*- */
/*
 * This file is part of WifiQrCode.
 *
 * main_window.vala
 *
 * Copyright (c) 2017-2018 Gianni Lerro {Squall77it} ~ <squall77it@gmail.com>
 *
 * WifiQrCode is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * WifiQrCode is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace WifiQrCode {

	[GtkTemplate (ui = "/org/gnome/wifi-qrcode/interface/main-window.ui")]
	public class ApplicationWindow : Gtk.ApplicationWindow
	{
		[GtkChild]
		private Gtk.ComboBoxText comboboxtextSSID;

		[GtkChild]
		private Gtk.Image imageQrCode;

		[GtkChild]
		private Gtk.MenuItem menuitemSaveAsPng;

		HashTable<string, string> wifi_connections_ht;

		public ApplicationWindow (Gtk.Application application)
		{
			GLib.Object (application: application,
				icon_name: "org.gnome.wifi-qrcode");

			var wifi_connections = new WifiConnections ();
			wifi_connections_ht = wifi_connections.connections_ht;

			//stdout.printf ("\n -- DEBUG main_window START -- \n\n");

			wifi_connections_ht.foreach ((key, val) => {
				comboboxtextSSID.append_text( (string) key);
				//stdout.printf ("\t%s \t => %s\n", (string) key, (string) val);
			});

			//stdout.printf ("\n -- DEBUG main_window END -- \n\n");
		}

		[GtkCallback]
		void on_menuitemSaveAsPng_activate ()
		{
			// If not work in Flatpak or Snap use Gtk.FileChooserNative
			Gtk.FileChooserDialog filechooserSaveAsPNG = new Gtk.FileChooserDialog (
				_("PNG Image") + " (*.png)", this, Gtk.FileChooserAction.SAVE,
				_("_Cancel"), Gtk.ResponseType.CANCEL,
				_("_Save"), Gtk.ResponseType.ACCEPT, null);

			filechooserSaveAsPNG.do_overwrite_confirmation = true;
			filechooserSaveAsPNG.set_current_name ("WifiQrCode.png");
			filechooserSaveAsPNG.set_icon_name ("wifi-qrcode");

			Gtk.FileFilter filefilterPNG = new Gtk.FileFilter ();
			filechooserSaveAsPNG.set_filter (filefilterPNG);
			filefilterPNG.add_mime_type ("image/png");

			if (filechooserSaveAsPNG.run () == Gtk.ResponseType.ACCEPT) {
				string _filename = filechooserSaveAsPNG.get_filename ();
				stdout.printf ("\tFile name selected: %s\n", _filename);

				Cairo.Surface img_QrCode_surface = imageQrCode.surface;
				img_QrCode_surface.write_to_png(_filename);
			}

			filechooserSaveAsPNG.close ();
		}

		[GtkCallback]
		void on_comboboxtextSSID_changed ()
		{
			menuitemSaveAsPng.set_sensitive (false);

			Qrencode.QRcode qrcode;

			unowned string connection_string = wifi_connections_ht.lookup (comboboxtextSSID.get_active_text ());

			qrcode = new Qrencode.QRcode.encodeString (connection_string, 0, Qrencode.EcLevel.M, Qrencode.Mode.B8, 1);

			//stdout.printf ("\tWifi String    --> %s\n", connection_string);
			//stdout.printf ("\tQrCode Version --> %d\n", qrcode.version);
			//stdout.printf ("\tQrcode Width  --> %d\n", qrcode.width);
			//stdout.printf ("\tQrCode Data  --> %s\n", (string) qrcode1.data);

			if (qrcode != null)
			{
				imageQrCode.set_from_surface (Draw_QrCode (qrcode.data, (uint16) qrcode.width, 2));

				menuitemSaveAsPng.set_sensitive (true);
			}
		}

		Cairo.ImageSurface Draw_QrCode(uint8[] qr_data, uint16 qr_width, uint8 qr_border)
		{
			const double MIN_SQUARE_SIZE = 1.0;
			const uint8 MIN_BORDER = 1;
			const uint16 WIDTH = 400;

			Cairo.ImageSurface surface = new Cairo.ImageSurface (Cairo.Format.ARGB32, WIDTH, WIDTH);
			Cairo.Context context = new Cairo.Context (surface);

			if (qr_border < MIN_BORDER)
				qr_border = MIN_BORDER;

			uint16 row_size = qr_border + qr_width + qr_border;
			double square_size = (double) WIDTH / (double) row_size;

			//stdout.printf ("\tQrcode Width  --> %d\n", qr_width);
			//stdout.printf ("\tQrcode Border --> %d\n", qr_border);
			//stdout.printf ("\tRow Size      --> %d\n", row_size);
			//stdout.printf ("\tSquare Size   --> %f\n\n", square_size);

			if (square_size < MIN_SQUARE_SIZE)
				square_size = MIN_SQUARE_SIZE;

			context.set_source_rgba (1.0, 1.0, 1.0, 1.0);
			context.rectangle( 0, 0, WIDTH, WIDTH);
			context.fill();

			for (int iy = qr_border; iy < (row_size - qr_border); iy++)
			{
				for (int ix = qr_border; ix < (row_size - qr_border); ix++)
				{
						if ((qr_data [(iy - qr_border) * qr_width + (ix - qr_border)] & 1) != 0)
						{
							context.set_source_rgba (0.0, 0.0, 0.0, 1.0);
							context.rectangle(ix * square_size, iy * square_size, square_size, square_size);
							context.fill_preserve();
						}
				}
			}
			return surface;
		}
	}
}
