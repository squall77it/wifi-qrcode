/* -*- Mode: Vala; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*- */
/*
 * This file is part of WifiQrCode.
 *
 * wifi-connections.vala
 *
 * Copyright (c) 2017-2018 Gianni Lerro {Squall77it} ~ <squall77it@gmail.com>
 *
 * WifiQrCode is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * WifiQrCode is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//using DBus;

namespace WifiQrCode {

	// Network Manager Interfaces
	[DBus (name = "org.freedesktop.NetworkManager.Settings")]
	interface Settings : GLib.Object
	{
		[DBus (name = "ListConnections")] // DBus signature = ao
		public abstract GLib.ObjectPath[] list_connections () throws GLib.DBusError, GLib.IOError;
	}

	[DBus (name = "org.freedesktop.NetworkManager.Settings.Connection")]
	interface Connection : GLib.Object
	{
		[DBus (name = "GetSecrets")] // DBus signature = a{sa{sv}}
		public abstract HashTable<string, HashTable<string, Variant>> get_secrets (string setting_name) throws GLib.DBusError, GLib.IOError;

		[DBus (name = "GetSettings")] // DBus signature = a{sa{sv}}
		public abstract HashTable<string, HashTable<unowned string, Variant>> get_settings () throws GLib.DBusError, GLib.IOError;
	}

	public class WifiConnections : GLib.Object
	{
		private HashTable<string, string> _connections_ht;

		private GLib.ObjectPath[] connection_paths = null;

		public HashTable<string, string> connections_ht { get { return _connections_ht; } }

		public WifiConnections ()
		{
			//stdout.printf ("\n -- DEBUG wifi_connections START -- \n\n");

			_connections_ht = new HashTable<string, string> (str_hash, str_equal);

			get_connections ();

			if (connection_paths != null)
			{
				foreach (var connection_path in connection_paths)
				{
					get_connection_settings (connection_path);
				}

				//stdout.printf ("\n -- DEBUG wifi_connections END -- \n\n");
			}
		}

		private void get_connections ()
		{
			Settings settings = null;

			try
			{
				// Ask the settings service for the list of connections it provides
				settings = Bus.get_proxy_sync (BusType.SYSTEM,
					"org.freedesktop.NetworkManager",
					"/org/freedesktop/NetworkManager/Settings");

				connection_paths = settings.list_connections ();
			}
			catch (GLib.DBusError e)
			{
				stderr.printf ("DBus Error: %s\n", e.message);
			}
		}

		private void get_connection_settings (GLib.ObjectPath connection_path)
		{
			HashTable<string, HashTable<unowned string, Variant>> connection_settings =
				new HashTable<string, HashTable<unowned string, Variant>> (str_hash, str_equal);

			HashTable<string, Variant> connection_setting =
				new HashTable<string, Variant> (str_hash, str_equal);

			HashTable<string, HashTable<string, Variant>> connection_secrets =
				new HashTable<string, HashTable<unowned string, Variant>> (str_hash, str_equal);

			HashTable<string, Variant> connection_secret =
				new HashTable<string, Variant> (str_hash, str_equal);

			Connection connection = null;

			string connection_name = "", connection_ssid = "";
			string connection_security_type = "", connection_password ="";
			string connection_hidden = "False";

			StringBuilder connection_string = new StringBuilder ();

			//stdout.printf ("\tConnection Path: %s\n\n", connection_path);

			try
			{
				// Ask the connection service for the settings it provides
				connection = Bus.get_proxy_sync (BusType.SYSTEM,
					"org.freedesktop.NetworkManager", connection_path);

				connection_settings = connection.get_settings ();

				if (connection_settings.contains ("802-11-wireless"))
				{

					connection_setting = connection_settings.get ("connection");
					connection_name = connection_setting.get ("id").get_string ();

					//stdout.printf ("\t\tConnection Name: %s\n", connection_name);

					connection_setting = connection_settings.get ("802-11-wireless");
					//connection_ssid = connection_setting.get ("ssid").get_bytestring ();
					//connection_ssid = connection_setting.get ("ssid").get_fixed_array ();
					connection_ssid = convert_to_null_terminated_string ((char[]) connection_setting.get ("ssid"));

					//stdout.printf ("\t\tConnection SSID: %s\n", connection_ssid);

					if (connection_setting.contains ("hidden"))
					{
						if (connection_setting.get ("hidden").get_boolean ())
						{
							connection_hidden = "True";
						}
					}

					//stdout.printf ("\t\tConnection Hidden: %s\n", connection_hidden);

					if (connection_settings.contains ("802-11-wireless-security"))
					{
						connection_setting = connection_settings.get ("802-11-wireless-security");

						// Ask the connection service for the secrets it provides
						connection_secrets = connection.get_secrets("802-11-wireless-security");
						connection_secret = connection_secrets.get ("802-11-wireless-security");

						if (connection_setting.get ("key-mgmt").get_string () == "wpa-psk")
						{
							connection_security_type = "WPA";
							connection_password = connection_secret.get ("psk").get_string ();
						}
						else
						{
							connection_security_type = "WEP";
							connection_password = connection_secret.get ("wep-key0").get_string ();
						}
					}
					else
					{
						connection_security_type = "nopass";
						connection_password = "";
					}

					//stdout.printf ("\t\tConnection Security Type: %s\n", connection_security_type);
					//stdout.printf ("\t\tConnection Password: %s\n", connection_password);

					// Wifi Network Configiguration (Android)
					// Example: WIFI:S:networkssid;T:WPA;P:password;H:false;;
					connection_string.assign("WIFI:");
					connection_string.append_printf("S:\"%s\";", connection_ssid);
					connection_string.append_printf("T:%s;",     connection_security_type);
					connection_string.append_printf("P:\"%s\";", connection_password);
					connection_string.append_printf("H:%s;;",    connection_hidden);

					//stdout.printf("\t\tConnection String: %s\n\n", connection_string.str);

					_connections_ht.insert(connection_name, connection_string.str);
				}
			}
			catch (GLib.DBusError e)
			{
				stderr.printf ("DBus Error: %s\n", e.message);
			}
		}

		// Convert a non null terminated string to a null terminated one
		private string convert_to_null_terminated_string (char[] buffer)
		{
			var temp_str = new StringBuilder ();

			foreach (var c in buffer)
			{
				temp_str.append_printf ("%c", c);
			}
			return temp_str.str;
		}
	}
}
